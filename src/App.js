import "./App.css";
import UserLayout from "./layouts/UserLayout";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import LoginForm from "./components/auth/LoginForm";
import PrivateRoute from "./components/auth/PrivateRoute";
import NotFound from "./layouts/NotFound";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/login">
          <LoginForm />
        </Route>
        <PrivateRoute path="/admin">
          <UserLayout />
        </PrivateRoute>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
