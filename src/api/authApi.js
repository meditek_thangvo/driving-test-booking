import axiosClient from "./axiosClient";

const authApi = {
  getCurrentUserLogin: () => {
    const url = `/auth`;
    return axiosClient.get(url);
  },
  login: (data) => {
    const url = "/auth";
    return axiosClient.post(url, data);
  },
  logout: (userId) => {
    const url = `/auth/logout/${userId}`;
    return axiosClient.post(url);
  },
};

export default authApi;
