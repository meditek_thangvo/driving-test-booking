import axiosClient from "./axiosClient";

const bookingApi = {
  getSiteList: (query) => {
    const url = `/bookingSite${query ? `?${query}` : ``}`;
    return axiosClient.get(url);
  },
  updateSite: (data) => {
    const url = `/bookingSite/${data._id}`;
    return axiosClient.put(url, data);
  },
  getSiteBookingTime: (query) => {
    const url = `/bookingTime${query ? `?${query}` : ``}`;
    return axiosClient.get(url);
  },
  createOneTime: (data) => {
    const url = `/bookingTime/${data.idSiteTime}`;
    return axiosClient.put(url, data);
  },
  deleteTime: (idSiteTime, idTime) => {
    const url = `/bookingTime/${idSiteTime}/${idTime}`;
    return axiosClient.delete(url);
  },
  updateTime: (idSiteTime, idTime, data) => {
    const url = `/bookingTime/${idSiteTime}/${idTime}`;
    return axiosClient.put(url, data);
  },
  createSiteBookingTime: (data) => {
    const url = `/bookingTime`;
    return axiosClient.post(url, data);
  },
  startPDA: () => {
    const url = `/pda/start`;
    return axiosClient.post(url);
  },
  stopPDA: () => {
    const url = `/pda/stop`;
    return axiosClient.post(url);
  },
};

export default bookingApi;
