import axiosClient from "./axiosClient";

const candidateApi = {
  getCandidateList: (query) => {
    const url = `/candidate${query ? `?${query}` : ``}`;
    return axiosClient.get(url);
  },
  getCandidateById: (candidateId) => {
    const url = `/candidate/${candidateId}`;
    return axiosClient.get(url);
  },
  createCandidate: (data) => {
    const url = "/candidate";
    return axiosClient.post(url, data);
  },
  updateCandidate: (data) => {
    const url = `/candidate/${data._id}`;
    return axiosClient.put(url, data);
  },
  deleteCandidate: (id) => {
    const url = `/candidate/${id}`;
    return axiosClient.delete(url);
  },
};

export default candidateApi;
