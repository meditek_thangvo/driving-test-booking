// @ts-nocheck
import { LoginOutlined } from "@ant-design/icons";
import { Button, Card, Col, Input, Row, Typography } from "antd";
import React, { useState } from "react";
import { Helmet } from "react-helmet";
import { useHistory, useRouteMatch } from "react-router-dom";
import authApi from "../../api/authApi";
import { setToken } from "../../api/axiosClient";
import { notificationFunction } from "../../utils";
import styles from "./LoginForm.module.less";

const { Text } = Typography;

export default function LoginForm(props) {
  const [valueInputs, setValueInput] = useState({ email: "", password: "" });

  const match = useRouteMatch();
  const history = useHistory();

  const handleOnChangeFields = (e, name) => {
    setValueInput({
      ...valueInputs,
      [name]: e.target.value,
    });
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();
    try {
      const result = await authApi.login(valueInputs);
      if (result.success) {
        localStorage.setItem("dataLogin", JSON.stringify(result.data));
        setToken(result.data.token);
        history.push(`/admin`);
      } else {
        notificationFunction("error", result.message);
      }
      console.log("result====", result);
    } catch (error) {}
  };

  return (
    <div className={styles.container}>
      <Helmet>
        <title>Login form</title>
      </Helmet>
      <div>
        <form onSubmit={handleSubmitForm}>
          <Card
            title="Login to Driving test booking"
            style={{ width: "35%", display: "inline-block" }}
          >
            <Col span={24} style={{ justifyContent: "left", display: "flex" }}>
              <div style={{ width: "80%", marginLeft: 48 }}>
                <Row style={{ marginBottom: 8 }}>
                  <Text>
                    Email<span style={{ color: "red" }}>(*)</span>:
                  </Text>
                </Row>
                <Row style={{ marginBottom: 20 }}>
                  <Input
                    required
                    onChange={(e) => handleOnChangeFields(e, "email")}
                    value={valueInputs.email}
                    placeholder="input email"
                  />
                </Row>
                <Row style={{ marginBottom: 8 }}>
                  <Text>
                    Password <span style={{ color: "red" }}>(*)</span>:
                  </Text>
                </Row>
                <Row style={{ marginBottom: 20 }}>
                  <Input.Password
                    required
                    onChange={(e) => handleOnChangeFields(e, "password")}
                    value={valueInputs.password}
                    placeholder="input password"
                  />
                </Row>

                <div style={{ textAlign: "right" }}>
                  <Button
                    type="primary"
                    htmlType="submit"
                    icon={<LoginOutlined />}
                    style={{ marginLeft: 8 }}
                  >
                    Login
                  </Button>
                </div>
              </div>
            </Col>
          </Card>
        </form>
      </div>
    </div>
  );
}
