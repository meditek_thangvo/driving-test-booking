import * as React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { setToken, removeToken } from "../../api/axiosClient";

export default function PrivateRoute(props) {
  const isLoggedIn = Boolean(localStorage.getItem("dataLogin"));
  if (!isLoggedIn) {
    return <Redirect to="login" />;
  } else {
    const dataLogin = JSON.parse(localStorage.getItem("dataLogin"));
    setToken(dataLogin.token);
  }

  return <Route {...props} />;
}
