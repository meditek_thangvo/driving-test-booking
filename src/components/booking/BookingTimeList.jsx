// @ts-nocheck
import { Col, Row, Spin } from "antd";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import bookingApi from "../../api/bookingApi";
import SiteList from "./SiteList";
import TimeList from "./TimeList";

export default function BookingTimeList() {
  const [siteSelected, setSiteSelected] = useState(null);
  const [dataSiteBookingTime, setDataSiteBookingTime] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleRowSiteClick = (site) => {
    setSiteSelected(site);
  };

  const getSiteBookingTime = async () => {
    try {
      setLoading(true);
      let query;
      const filterOptions = { SITE: siteSelected.SITE };
      if (filterOptions !== null)
        query = queryString.stringify(filterOptions, {
          skipEmptyString: true,
          skipNull: true,
        });
      console.log("query================is", query);

      const result = await bookingApi.getSiteBookingTime(query);

      console.log("result====", result);
      if (result && result.data && result.data.length > 0) {
        setDataSiteBookingTime(result.data[0]);
      } else {
        setDataSiteBookingTime([]);
      }

      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (siteSelected) {
      getSiteBookingTime();
    }
  }, [siteSelected]);

  const clearBookingTimes = () => {
    setDataSiteBookingTime([]);
  };

  const handleCallBackDeleteTime = () => {
    getSiteBookingTime();
  };

  return (
    <div style={{ margin: "24px 24px" }}>
      <Helmet>
        <title>Booking time</title>
      </Helmet>
      <Spin spinning={loading}>
        <Row span={24} style={{ justifyContent: "space-between" }}>
          <Col span={6}>
            <SiteList
              handleRowSiteClick={handleRowSiteClick}
              clearBookingTimes={clearBookingTimes}
            />
          </Col>
          <Col span={17}>
            <TimeList
              dataTimes={dataSiteBookingTime}
              siteSelected={siteSelected}
              handleCallBackDeleteTime={handleCallBackDeleteTime}
            />
          </Col>
        </Row>
      </Spin>
    </div>
  );
}
