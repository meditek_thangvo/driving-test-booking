// @ts-nocheck
import { Checkbox, Modal, notification, Spin, Table, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import styled from "styled-components";
import bookingApi from "../../api/bookingApi";
import "./stylesBooking.css";
import { notificationFunction } from "../../utils";

const Wrapper = styled.div`
  tr.ant-table-row-level-0:hover > td {
    background: #f5dfb382;
  }
`;

const { Text } = Typography;

export default function SiteList(props) {
  const { handleRowSiteClick, clearBookingTimes } = props;
  const [siteList, setSiteList] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedSite, setSelectedSite] = useState(null);
  const [loading, setLoading] = useState(false);
  const match = useRouteMatch();
  const history = useHistory();

  const columns = [
    {
      title: "#",
      key: "index",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 30,
    },
    {
      title: "Site name",
      dataIndex: "SITE",
      key: "SITE",
      width: 90,
    },
    {
      title: "Status",
      key: "ENABLED",
      render: (text, record) => (
        <Checkbox
          checked={record.ENABLED}
          onClick={(e) => {
            e.stopPropagation();
          }}
          onChange={(e) => onChangeCheckSite(e, record)}
        />
      ),
      width: 60,
    },
  ];

  const onChangeCheckSite = (e, record) => {
    setIsModalVisible(true);
    setSelectedSite(record);
  };

  // {$and:[{LICENSE_NUMBER:{$eq: "8142593"}}, {LICENSE_FIRST_NAME: {$eq: "David"}}] }

  const getSiteList = async () => {
    try {
      setLoading(true);

      const result = await bookingApi.getSiteList();

      console.log("result====", result);
      setSiteList(result.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getSiteList();
  }, []);

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleUpdateSite = async () => {
    setIsModalVisible(false);
    const dataUpdate = {
      ...selectedSite,
      ENABLED: !selectedSite.ENABLED,
    };
    try {
      const result = await bookingApi.updateSite(dataUpdate);
      console.log("result update====", result);
      if (result.success === true) {
        notificationFunction("success", result.message);
        getSiteList();
        console.log("====!!selectedSite.ENABLED====", !!selectedSite.ENABLED);
        if (!!selectedSite.ENABLED) {
          clearBookingTimes();
        } else {
          handleRowSiteClick(selectedSite);
        }
      } else {
        notificationFunction("error", result.message);
      }
    } catch (error) {
      notificationFunction("error", "Updating failed");
    }
  };

  const handleChangeSelectedRowSite = (record) => {
    console.log("record==========", record);
    if (record.ENABLED === true) {
      setSelectedSite(record);
      handleRowSiteClick(record);
    }
  };

  return (
    <Wrapper className="table-site-container">
      <Spin spinning={loading}>
        <Text style={{ marginBottom: 8, fontWeight: "bold", display: "block" }}>
          Site list
        </Text>
        <Table
          columns={columns}
          dataSource={siteList}
          pagination={false}
          onRow={(record) => ({
            onClick: () => {
              handleChangeSelectedRowSite(record);
            },
          })}
          scroll={{ y: 500 }}
          rowClassName={(record, index) =>
            selectedSite && selectedSite.CODE === record.CODE
              ? "row-selected"
              : ""
          }
        />
      </Spin>
      <Modal
        title={`${
          selectedSite && selectedSite.ENABLED ? `UNENABLE` : `ENABLE`
        } site ${selectedSite ? selectedSite.SITE : ``}`}
        visible={isModalVisible}
        onOk={handleUpdateSite}
        onCancel={handleCancel}
      >
        <p>
          {selectedSite
            ? `Are you sure to ${
                selectedSite.ENABLED ? `UNENABLE` : `ENABLE`
              } the site ${selectedSite.SITE} ?`
            : ""}
        </p>
      </Modal>
    </Wrapper>
  );
}
