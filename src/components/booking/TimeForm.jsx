// @ts-nocheck
import { CloseCircleOutlined, SaveOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  DatePicker,
  Row,
  TimePicker,
  Typography,
  Checkbox,
} from "antd";
import moment from "moment";
import React, { useState } from "react";

const { Text } = Typography;
const dateFormat = "DD/MM/YYYY";

export default function TimeForm(props) {
  const { initialValues, onSubmit, onCancel } = props;

  console.log("initialValues", initialValues);

  const [valueInputs, setValueInput] = useState(initialValues);

  const handleOnChangeFromDate = (date, dateString) => {
    console.log("date===========>", date);
    console.log("dateString===========>", dateString);
    setValueInput({
      ...valueInputs,
      FROM_DATE: moment(date, "DD/MM/YYYY"),
      FROM_DATE_STRING: dateString,
    });
  };
  const handleOnChangeToDate = (date, dateString) => {
    setValueInput({
      ...valueInputs,
      TO_DATE: moment(date, "DD/MM/YYYY"),
      TO_DATE_STRING: dateString,
    });
  };

  const onChangeFromTime = (time, timeString) => {
    console.log("from time", time, timeString);
    setValueInput({
      ...valueInputs,
      FROM_TIME: moment(time, "DD/MM/YYYY h A"),
      FROM_TIME_STRING: timeString.toUpperCase(),
    });
  };
  const onChangeToTime = (time, timeString) => {
    console.log("to time", time, timeString);
    setValueInput({
      ...valueInputs,
      TO_TIME: moment(time, "DD/MM/YYYY h A"),
      TO_TIME_STRING: timeString.toUpperCase(),
    });
  };

  const handleSubmitForm = (e) => {
    e.preventDefault();
    console.log("valueInputs============", valueInputs);
    onSubmit(valueInputs);
  };

  const handleCancel = () => {
    onCancel();
  };

  const handleOnChangeCheckbox = (e) => {
    setValueInput({
      ...valueInputs,
      ISBOOKED: e.target.checked,
    });
  };

  return (
    <div>
      <form onSubmit={handleSubmitForm}>
        <Col span={24} style={{ justifyContent: "left", display: "flex" }}>
          <div style={{ width: "100%", marginLeft: 48 }}>
            <Row style={{ marginBottom: 24 }}>
              <Text>
                Site{" "}
                <span style={{ fontWeight: "bold" }}>{valueInputs.site}</span>
              </Text>
            </Row>
            <Row>
              <Col span={12}>
                <Row style={{ marginBottom: 8 }}>
                  <Text>
                    From date <span style={{ color: "red" }}>(*)</span>:
                  </Text>
                </Row>
                <Row style={{ marginBottom: 24 }}>
                  <DatePicker
                    style={{ width: 130 }}
                    onChange={handleOnChangeFromDate}
                    required
                    value={moment(valueInputs.FROM_DATE, "DD/MM/YYYY")}
                    format={dateFormat}
                  />
                </Row>
              </Col>
              <Col span={12}>
                <Row style={{ marginBottom: 8 }}>
                  <Text>
                    To date <span style={{ color: "red" }}>(*)</span>:
                  </Text>
                </Row>
                <Row style={{ marginBottom: 24 }}>
                  <DatePicker
                    style={{ width: 130 }}
                    onChange={handleOnChangeToDate}
                    required
                    value={moment(valueInputs.TO_DATE, "DD/MM/YYYY")}
                    format={dateFormat}
                  />
                </Row>
              </Col>
            </Row>

            <Row>
              <Col span={12}>
                <Row style={{ marginBottom: 8 }}>
                  <Text>
                    From time <span style={{ color: "red" }}>(*)</span>:
                  </Text>
                </Row>
                <Row style={{ marginBottom: 24 }}>
                  <TimePicker
                    use12Hours
                    format="hh a"
                    onChange={onChangeFromTime}
                    required
                    value={valueInputs.FROM_TIME}
                    style={{ width: 130 }}
                  />
                </Row>
              </Col>
              <Col span={12}>
                <Row style={{ marginBottom: 8 }}>
                  <Text>
                    To time <span style={{ color: "red" }}>(*)</span>:
                  </Text>
                </Row>
                <Row style={{ marginBottom: 24 }}>
                  <TimePicker
                    use12Hours
                    format="hh a"
                    onChange={onChangeToTime}
                    required
                    value={valueInputs.TO_TIME}
                    style={{ width: 130 }}
                  />
                </Row>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Row style={{ marginBottom: 24 }}>
                  <Checkbox
                    checked={valueInputs.ISBOOKED}
                    onChange={handleOnChangeCheckbox}
                  />
                </Row>
              </Col>
            </Row>

            <div style={{ textAlign: "right" }}>
              <Button icon={<CloseCircleOutlined />} onClick={handleCancel}>
                Cancel
              </Button>

              <Button
                type="primary"
                htmlType="submit"
                icon={<SaveOutlined />}
                style={{ marginLeft: 8 }}
              >
                Save
              </Button>
            </div>
          </div>
        </Col>
      </form>
    </div>
  );
}
