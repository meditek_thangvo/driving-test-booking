// @ts-nocheck
import { Table, Typography, Space, Checkbox, Modal, Row, Col } from "antd";
import React, { useState } from "react";
import moment from "moment";
import bookingApi from "../../api/bookingApi";
import { notificationFunction } from "../../utils";
import TimeForm from "./TimeForm";

const { Text } = Typography;

export default function TimeList(props) {
  const { siteSelected, dataTimes, handleCallBackDeleteTime } = props;

  console.log("dataTimes========la", dataTimes);

  const [selectedTime, setSelectedTime] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isModalEditVisible, setIsModalEditVisible] = useState(false);

  const columns = [
    {
      title: "#",
      key: "index",
      render: (text, record, index) => {
        return index + 1;
      },
      width: 50,
    },
    {
      title: "From date",
      dataIndex: "FROM_DATE",
      key: "FROM_DATE",
      width: 110,
    },
    {
      title: "To date",
      dataIndex: "TO_DATE",
      key: "TO_DATE",
      width: 110,
    },
    {
      title: "From time",
      dataIndex: "FROM_TIME",
      key: "FROM_TIME",
      width: 100,
    },
    {
      title: "To time",
      dataIndex: "TO_TIME",
      key: "TO_TIME",
      width: 100,
    },
    {
      title: "Isbooked",
      dataIndex: "ISBOOKED",
      key: "ISBOOKED",
      render: (text, record, index) => {
        return <Checkbox checked={text} disabled={true}></Checkbox>;
      },
      width: 100,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <a
            style={{ color: "#1890ff", textDecoration: "underline" }}
            onClick={(e) => handleClickEditTime(record)}
          >
            Edit
          </a>
          <a
            style={{ color: "#1890ff", textDecoration: "underline" }}
            onClick={(e) => handleClickDeleteTime(record)}
          >
            Delete
          </a>
        </Space>
      ),
    },
  ];

  const handleDeleteTime = async (record) => {
    setIsModalVisible(false);
    try {
      console.log("dataTimes 222222========la", dataTimes);
      const result = await bookingApi.deleteTime(
        dataTimes._id,
        selectedTime._id
      );
      console.log("result delete====", result);
      if (result.success === true) {
        notificationFunction("success", result.message);
        handleCallBackDeleteTime();
      } else {
        notificationFunction("error", result.message);
      }
    } catch (error) {
      console.log("error delete time", error);
      notificationFunction("error", "Deleting failed");
    }
  };
  const handleSaveTime = async (inputValues) => {
    console.log("=====inputValues to update=====", inputValues);
    setIsModalEditVisible(false);
    const dataUpdate = {
      FROM_DATE: moment(inputValues.FROM_DATE, "YYYY-MM-DD").format(
        "DD/MM/YYYY"
      ),
      TO_DATE: moment(inputValues.TO_DATE, "YYYY-MM-DD").format("DD/MM/YYYY"),
      FROM_TIME: inputValues.FROM_TIME_STRING,
      TO_TIME: inputValues.TO_TIME_STRING,
      ISBOOKED: inputValues.ISBOOKED,
    };
    try {
      let result;
      if (inputValues.isCreate === "create_time") {
        result = await bookingApi.createOneTime({
          idSiteTime: dataTimes._id,
          ...dataUpdate,
        });
      } else if (inputValues.isCreate === "create_site_time") {
        result = await bookingApi.createSiteBookingTime({
          SITE: siteSelected.SITE,
          TIMES: [
            {
              ...dataUpdate,
            },
          ],
        });
      } else {
        result = await bookingApi.updateTime(
          dataTimes._id,
          selectedTime._id,
          dataUpdate
        );
      }

      console.log("result edit====", result);
      if (result.success === true) {
        notificationFunction("success", result.message);
        handleCallBackDeleteTime();
      } else {
        notificationFunction("error", result.message);
      }
    } catch (error) {
      console.log("error edit time", error);
      notificationFunction("error", "Saving failed");
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleClickDeleteTime = (record) => {
    setSelectedTime(record);
    setIsModalVisible(true);
  };

  const handleCancelEdit = () => {
    setIsModalEditVisible(false);
  };

  const handleClickEditTime = (record) => {
    console.log("record time", record);
    setSelectedTime(record);
    setIsModalEditVisible(true);
  };

  const handleClickCreateBookingTime = () => {
    setIsModalEditVisible(true);
    setSelectedTime({
      FROM_DATE: moment(new Date()),
      TO_DATE: moment(new Date()),
      FROM_TIME: "",
      TO_TIME: "",
      FROM_TIME_STRING: "12 PM",
      TO_TIME_STRING: "12 PM",

      site: dataTimes ? dataTimes.SITE : "",
      isCreate: dataTimes && dataTimes._id ? "create_time" : "create_site_time",
    });
  };

  return (
    <div>
      <Row>
        <Col span={18}>
          <Text
            style={{ marginBottom: 8, fontWeight: "bold", display: "block" }}
          >
            Booking time of site{" "}
            <span style={{ color: "red", fontWeight: "bold" }}>{`${
              siteSelected ? siteSelected.SITE : ""
            }`}</span>
          </Text>
        </Col>
        <Col span={6}>
          <p
            style={{ color: "#1890ff", fontWeight: "bold", cursor: "pointer" }}
            onClick={handleClickCreateBookingTime}
          >
            Create booking time
          </p>
        </Col>
      </Row>

      <Table
        columns={columns}
        dataSource={dataTimes ? dataTimes.TIMES : []}
        scroll={{ y: 500 }}
        style={{ height: "100vh", overflow: "scroll", display: "block" }}
      />
      <Modal
        title="Delete booking time"
        visible={isModalVisible}
        onOk={handleDeleteTime}
        onCancel={handleCancel}
      >
        <p>{`Are you sure to delete the booking time from ${
          selectedTime ? selectedTime.FROM_TIME : ``
        } to ${selectedTime ? selectedTime.TO_TIME : ``}?`}</p>
      </Modal>
      {isModalEditVisible ? (
        <Modal
          title="Booking time info"
          visible={isModalEditVisible}
          footer={null}
        >
          <TimeForm
            initialValues={{
              ...selectedTime,
              FROM_TIME: moment(
                `03/03/2021 ${selectedTime ? selectedTime.FROM_TIME : ``}`,
                "DD/MM/YYYY h A"
              ),
              TO_TIME: moment(
                `03/03/2021 ${selectedTime ? selectedTime.TO_TIME : ``}`,
                "DD/MM/YYYY h A"
              ),
              FROM_TIME_STRING: selectedTime ? selectedTime.FROM_TIME : "",
              TO_TIME_STRING: selectedTime ? selectedTime.TO_TIME : "",
              site: dataTimes ? dataTimes.SITE : "",
            }}
            onSubmit={handleSaveTime}
            onCancel={handleCancelEdit}
          />
        </Modal>
      ) : null}
    </div>
  );
}
