// @ts-nocheck
import { CloseCircleOutlined, SaveOutlined } from "@ant-design/icons";
import { Button, Card, Col, DatePicker, Input, Row, Typography } from "antd";
import moment from "moment";
import React, { useState } from "react";
import { Helmet } from "react-helmet";

const { Text } = Typography;
const dateFormat = "DD/MM/YYYY";

export default function CandidateForm(props) {
  const { initialValues, onSubmit, onCancel } = props;

  const [valueInputs, setValueInput] = useState(initialValues);

  const handleOnChangeFields = (e, name) => {
    setValueInput({
      ...valueInputs,
      [name]: e.target.value,
    });
  };
  const handleOnChangeExpiryDate = (date, dateString) => {
    setValueInput({
      ...valueInputs,
      LICENSE_EXPIRY_DATE: moment(date, "DD/MM/YYYY"),
    });
  };
  const handleOnChangeBirthDay = (date, dateString) => {
    setValueInput({
      ...valueInputs,
      LICENSE_DATE_OF_BIRTH: moment(date, "DD/MM/YYYY"),
    });
  };

  const handleSubmitForm = (e) => {
    e.preventDefault();
    onSubmit(valueInputs);
  };

  const handleCancel = () => {
    onCancel();
  };

  return (
    <div style={{ overflow: "scroll" }}>
      <Helmet>
        <title>Candiate form</title>
      </Helmet>
      <form onSubmit={handleSubmitForm}>
        <Card title="Candidate information" style={{ width: "100%" }}>
          <Col span={24} style={{ justifyContent: "left", display: "flex" }}>
            <div style={{ width: "30%", marginLeft: 48 }}>
              <Row style={{ marginBottom: 8 }}>
                <Text>
                  License number <span style={{ color: "red" }}>(*)</span>:
                </Text>
              </Row>
              <Row style={{ marginBottom: 20 }}>
                <Input
                  required
                  onChange={(e) => handleOnChangeFields(e, "LICENSE_NUMBER")}
                  value={valueInputs.LICENSE_NUMBER}
                />
              </Row>
              <Row style={{ marginBottom: 8 }}>
                <Text>
                  License expiry date <span style={{ color: "red" }}>(*)</span>:
                </Text>
              </Row>
              <Row style={{ marginBottom: 20 }}>
                <DatePicker
                  style={{ width: 193 }}
                  onChange={handleOnChangeExpiryDate}
                  required
                  value={moment(valueInputs.LICENSE_EXPIRY_DATE, "DD/MM/YYYY")}
                  format={dateFormat}
                />
              </Row>
              <Row style={{ marginBottom: 8 }}>
                <Text>
                  License first name <span style={{ color: "red" }}>(*)</span>:
                </Text>
              </Row>
              <Row style={{ marginBottom: 20 }}>
                <Input
                  required
                  onChange={(e) =>
                    handleOnChangeFields(e, "LICENSE_FIRST_NAME")
                  }
                  value={valueInputs.LICENSE_FIRST_NAME}
                />
              </Row>
              <Row style={{ marginBottom: 8 }}>
                <Text>
                  License last name <span style={{ color: "red" }}>(*)</span>:
                </Text>
              </Row>
              <Row style={{ marginBottom: 20 }}>
                <Input
                  required
                  onChange={(e) => handleOnChangeFields(e, "LICENSE_LAST_NAME")}
                  value={valueInputs.LICENSE_LAST_NAME}
                />
              </Row>
              <Row style={{ marginBottom: 8 }}>
                <Text>
                  License date of birth{" "}
                  <span style={{ color: "red" }}>(*)</span>:
                </Text>
              </Row>
              <Row style={{ marginBottom: 24 }}>
                <DatePicker
                  value={moment(
                    valueInputs.LICENSE_DATE_OF_BIRTH,
                    "DD/MM/YYYY"
                  )}
                  format={dateFormat}
                  style={{ width: 193 }}
                  onChange={handleOnChangeBirthDay}
                  required
                />
              </Row>
              <div style={{ textAlign: "right" }}>
                <Button icon={<CloseCircleOutlined />} onClick={handleCancel}>
                  Cancel
                </Button>

                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<SaveOutlined />}
                  style={{ marginLeft: 8 }}
                >
                  Save
                </Button>
              </div>
            </div>
          </Col>
        </Card>
      </form>
    </div>
  );
}
