// @ts-nocheck
import { Checkbox, Modal, Space, Spin, Table } from "antd";
import moment from "moment";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useHistory, useRouteMatch } from "react-router-dom";
import candidateApi from "../../api/candidateApi";
import { notificationFunction } from "../../utils";
import SearchCandidate from "./SearchCandidate";

export default function CandidateList() {
  const [candidateList, setCandidateList] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedCandidate, setSelectedCandidate] = useState(null);
  const [loading, setLoading] = useState(false);
  const match = useRouteMatch();
  console.log("match is ======", match);
  const history = useHistory();

  const columns = [
    {
      title: "#",
      key: "index",
      render: (text, record, index) => {
        return candidateList.indexOf(record) + 1;
      },
    },
    {
      title: "License number",
      dataIndex: "LICENSE_NUMBER",
      key: "LICENSE_NUMBER",
    },
    {
      title: "Expiry date",
      dataIndex: "LICENSE_EXPIRY_DATE",
      key: "LICENSE_EXPIRY_DATE",
    },
    {
      title: "First name",
      dataIndex: "LICENSE_FIRST_NAME",
      key: "LICENSE_FIRST_NAME",
    },
    {
      title: "Last name",
      dataIndex: "LICENSE_LAST_NAME",
      key: "LICENSE_LAST_NAME",
    },
    {
      title: "Date of birth",
      dataIndex: "LICENSE_DATE_OF_BIRTH",
      key: "LICENSE_DATE_OF_BIRTH",
    },
    {
      title: "Is booked",
      dataIndex: "ISBOOKED",
      key: "ISBOOKED",
      render: (value) => <Checkbox checked={value} disabled={true} />,
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <a
            style={{ color: "#1890ff", textDecoration: "underline" }}
            onClick={(e) => handleClickEditCandidate(record)}
          >
            Edit
          </a>
          <a
            style={{ color: "#1890ff", textDecoration: "underline" }}
            onClick={(e) => handleClickDeleteOnTable(record)}
          >
            Delete
          </a>
        </Space>
      ),
    },
  ];

  // {$and:[{LICENSE_NUMBER:{$eq: "8142593"}}, {LICENSE_FIRST_NAME: {$eq: "David"}}] }

  const getCandidateList = async (filterOptions = null) => {
    try {
      setLoading(true);
      let query;
      if (filterOptions !== null)
        query = queryString.stringify(filterOptions, {
          skipEmptyString: true,
          skipNull: true,
        });
      console.log("query================is", query);

      const result = await candidateApi.getCandidateList(query);

      console.log("result====", result);
      setCandidateList(result.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCandidateList();
  }, []);

  const handleDeleteCandidate = async () => {
    setIsModalVisible(false);
    try {
      const result = await candidateApi.deleteCandidate(selectedCandidate._id);
      console.log("result delete====", result);
      if (result.success === true) {
        notificationFunction("success", result.message);
        getCandidateList();
      } else {
        notificationFunction("error", result.message);
      }
    } catch (error) {
      notificationFunction("error", "Deleting failed");
    }
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleClickDeleteOnTable = (record) => {
    setSelectedCandidate(record);
    setIsModalVisible(true);
  };

  const handleClickEditCandidate = (record) => {
    console.log();
    setSelectedCandidate(record);
    history.push(`${match.url}/${record._id}`);
  };

  const handleSearch = (filterOptions) => {
    getCandidateList(filterOptions);
  };

  return (
    <div style={{ padding: "24px 24px" }}>
      <Helmet>
        <title>Candidate list</title>
      </Helmet>

      <SearchCandidate handleSearch={handleSearch} />
      <Spin spinning={loading}>
        <Table columns={columns} dataSource={candidateList} />
      </Spin>
      <Modal
        title="Delete candidate"
        visible={isModalVisible}
        onOk={handleDeleteCandidate}
        onCancel={handleCancel}
      >
        <p>{`Are you sure to delete the candidate ${
          selectedCandidate ? selectedCandidate.LICENSE_FIRST_NAME : ""
        } ${selectedCandidate ? selectedCandidate.LICENSE_LAST_NAME : ""}?`}</p>
        <p>{`Date of birth: ${
          selectedCandidate
            ? moment(selectedCandidate.LICENSE_DATE_OF_BIRTH).format(
                "DD/MM/YYYY"
              )
            : ""
        }`}</p>
        <p>{`License number: ${
          selectedCandidate ? selectedCandidate.LICENSE_NUMBER : ""
        }`}</p>
        <p>{`License expiry date: ${
          selectedCandidate
            ? moment(selectedCandidate.LICENSE_EXPIRY_DATE).format("DD/MM/YYYY")
            : ""
        }`}</p>
      </Modal>
    </div>
  );
}
