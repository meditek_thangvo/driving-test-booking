// @ts-nocheck
import { notification, Spin, Typography } from "antd";
import { LeftCircleOutlined } from "@ant-design/icons";

import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import candidateApi from "../../api/candidateApi";
import CandidateForm from "./CandidateForm";
import { Link, useHistory } from "react-router-dom";
import { notificationFunction } from "../../utils";
import moment from "moment";

const { Text } = Typography;

export default function CreateUpdateCandidate(props) {
  const { candidateId } = useParams();

  const history = useHistory();

  const isEdit = candidateId ? true : false;
  const [candidate, setCandidate] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!candidateId) return;

    (async () => {
      try {
        const dataCandidate = await candidateApi.getCandidateById(candidateId);
        console.log("dataCandidate========", dataCandidate);
        setCandidate(dataCandidate.data);
      } catch (error) {
        console.log("failed to fetch candidate details", error);
      }
    })();
  }, []);

  const createCandidate = async (valueObj) => {
    try {
      setLoading(true);
      const dataCreate = {
        ...valueObj,
        LICENSE_EXPIRY_DATE: moment(
          valueObj.LICENSE_EXPIRY_DATE,
          "YYYY-MM-DD"
        ).format("DD/MM/YYYY"),
        LICENSE_DATE_OF_BIRTH: moment(
          valueObj.LICENSE_DATE_OF_BIRTH,
          "YYYY-MM-DD"
        ).format("DD/MM/YYYY"),
      };
      const result = await candidateApi.createCandidate(dataCreate);
      console.log("result====", result);
      if (result.success === true) {
        notificationFunction("success", result.message);
      } else {
        notificationFunction("error", result.message);
      }
      setLoading(false);
      history.push("/admin/candidate");
    } catch (error) {
      setLoading(false);
      notificationFunction("error", "Creating failed");
    }
  };

  const updateCandidate = async (valueObj) => {
    try {
      console.log("valueObj====", valueObj);
      setLoading(true);

      const dataUpdate = {
        ...valueObj,
        LICENSE_EXPIRY_DATE: moment(
          valueObj.LICENSE_EXPIRY_DATE,
          "YYYY-MM-DD"
        ).format("DD/MM/YYYY"),
        LICENSE_DATE_OF_BIRTH: moment(
          valueObj.LICENSE_DATE_OF_BIRTH,
          "YYYY-MM-DD"
        ).format("DD/MM/YYYY"),
      };

      const result = await candidateApi.updateCandidate(dataUpdate);
      console.log("result====", result);
      if (result.success === true) {
        notificationFunction("success", result.message);
      } else {
        notificationFunction("error", result.message);
      }
      setLoading(false);
      history.push("/admin/candidate");
    } catch (error) {
      setLoading(false);
      notificationFunction("error", "Creating failed");
    }
  };

  const handleSubmitForm = (formValues) => {
    if (isEdit) {
      updateCandidate(formValues);
    } else {
      createCandidate(formValues);
    }
  };

  const handleCancel = (formValues) => {
    history.push("/candidate");
  };

  const initialValues = {
    LICENSE_NUMBER: null,
    LICENSE_EXPIRY_DATE: moment(new Date()),
    LICENSE_FIRST_NAME: "",
    LICENSE_LAST_NAME: "",
    LICENSE_DATE_OF_BIRTH: moment(new Date()),
    ISBOOKED: false,
    ...candidate,
  };

  return (
    <div style={{ backgroundColor: "white" }}>
      <Spin spinning={loading}>
        <div style={{ margin: "13px 8px" }}>
          <Link to="/admin/candidate" exact>
            <LeftCircleOutlined />
            <Text style={{ cursor: "pointer", color: "#2f97fe" }}>
              {" "}
              Back to candidate list
            </Text>
          </Link>
        </div>

        {(!isEdit || Boolean(candidate)) && (
          <CandidateForm
            initialValues={initialValues}
            onSubmit={handleSubmitForm}
            onCancel={handleCancel}
          ></CandidateForm>
        )}
      </Spin>
    </div>
  );
}

// color: '#2f97fe'
