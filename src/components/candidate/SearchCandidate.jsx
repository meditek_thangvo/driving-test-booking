// @ts-nocheck
import React, { useState } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Checkbox, Input, Row, Col, DatePicker } from "antd";
import moment from "moment";

const dateFormat = "DD/MM/YYYY";

export default function SearchCandidate(props) {
  const { handleSearch } = props;
  const [valueInput, setValueInput] = useState({
    LICENSE_NUMBER: "",
    LICENSE_FIRST_NAME: "",
    LICENSE_LAST_NAME: "",
    LICENSE_EXPIRY_DATE: null,
    LICENSE_DATE_OF_BIRTH: null,
    ISBOOKED: null,
  });

  const handleOnChangeFields = (e, name) => {
    setValueInput({
      ...valueInput,
      [name]: e.target.value,
    });
  };
  const handleOnChangeExpiryDate = (date, dateString) => {
    setValueInput({
      ...valueInput,
      LICENSE_EXPIRY_DATE: moment(date).format("YYYY-MM-DD"),
    });
  };
  const handleOnChangeBirthDay = (date, dateString) => {
    setValueInput({
      ...valueInput,
      LICENSE_DATE_OF_BIRTH: moment(date).format("YYYY-MM-DD"),
    });
  };
  const handleOnChangeCheckbox = (e) => {
    setValueInput({
      ...valueInput,
      ISBOOKED: e.target.checked,
    });
  };

  const handleSearchCandidate = () => {
    console.log("value input is", valueInput);
    if (
      JSON.stringify(valueInput) !==
      JSON.stringify({
        LICENSE_NUMBER: "",
        LICENSE_FIRST_NAME: "",
        LICENSE_LAST_NAME: "",
        LICENSE_EXPIRY_DATE: null,
        LICENSE_DATE_OF_BIRTH: null,
        ISBOOKED: null,
      })
    ) {
      handleSearch(valueInput);
    }
  };
  const handleClearSearch = () => {
    if (
      JSON.stringify(valueInput) !==
      JSON.stringify({
        LICENSE_NUMBER: "",
        LICENSE_FIRST_NAME: "",
        LICENSE_LAST_NAME: "",
        LICENSE_EXPIRY_DATE: null,
        LICENSE_DATE_OF_BIRTH: null,
        ISBOOKED: null,
      })
    ) {
      setValueInput({
        LICENSE_NUMBER: "",
        LICENSE_FIRST_NAME: "",
        LICENSE_LAST_NAME: "",
        LICENSE_EXPIRY_DATE: null,
        LICENSE_DATE_OF_BIRTH: null,
        ISBOOKED: null,
      });
      handleSearch({
        LICENSE_NUMBER: "",
        LICENSE_FIRST_NAME: "",
        LICENSE_LAST_NAME: "",
        LICENSE_EXPIRY_DATE: null,
        LICENSE_DATE_OF_BIRTH: null,
        ISBOOKED: null,
      });
    }
  };
  return (
    <div style={{ marginBottom: 24 }}>
      <Row>
        <Col span={20}>
          <Row
            gutter={24}
            style={{ marginBottom: 8, justifyContent: "center" }}
          >
            <Col span={6}>
              <Input
                placeholder="License number"
                value={valueInput.LICENSE_NUMBER}
                onChange={(e) => handleOnChangeFields(e, "LICENSE_NUMBER")}
              />
            </Col>
            <Col span={6}>
              <DatePicker
                placeholder="License expiry date"
                value={valueInput.LICENSE_EXPIRY_DATE}
                format={dateFormat}
                onChange={handleOnChangeExpiryDate}
                style={{ width: 193 }}
              />
            </Col>
            <Col span={6}>
              <DatePicker
                placeholder="Date of birth"
                value={valueInput.LICENSE_DATE_OF_BIRTH}
                format={dateFormat}
                onChange={handleOnChangeBirthDay}
                style={{ width: 193 }}
              />
            </Col>
          </Row>
          <Row gutter={24} style={{ justifyContent: "center" }}>
            <Col span={6}>
              <Input
                placeholder="First name"
                value={valueInput.LICENSE_FIRST_NAME}
                onChange={(e) => handleOnChangeFields(e, "LICENSE_FIRST_NAME")}
              />
            </Col>
            <Col span={6}>
              <Input
                placeholder="Last name"
                value={valueInput.LICENSE_LAST_NAME}
                onChange={(e) => handleOnChangeFields(e, "LICENSE_LAST_NAME")}
              />
            </Col>
            <Col span={6}>
              <Checkbox onChange={handleOnChangeCheckbox}>Is booked</Checkbox>
            </Col>
          </Row>
        </Col>
        <Col span={4} style={{ textAlign: "left" }} flex="-200px">
          <Row>
            <Button
              type="primary"
              icon={<SearchOutlined />}
              onClick={handleSearchCandidate}
            >
              Search
            </Button>
          </Row>
          <Row style={{ marginTop: 8 }}>
            <Button onClick={handleClearSearch}>Clear</Button>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
