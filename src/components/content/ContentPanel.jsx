import React from "react";
// @ts-ignore
import styles from "./ContentPanel.module.less";
import { Helmet } from "react-helmet";

export default function ContentPanel() {
  return (
    <div className={styles.container}>
      <Helmet>
        <title>Driving test booking</title>
      </Helmet>
    </div>
  );
}
