// @ts-nocheck
import { Layout, Row, Col, Button } from "antd";
import React, { useEffect, useState } from "react";
import driving_log from "../../assets/img/driving_logo.png";
// @ts-ignore
import styles from "./HeaderBar.module.less";
import { Link, Redirect } from "react-router-dom";
import { useHistory, useRouteMatch } from "react-router-dom";
import authApi from "../../api/authApi";

const { Header } = Layout;

export default function HeaderBar() {
  const [dataUser, setDataUser] = useState({});
  const match = useRouteMatch();
  console.log("match is ======", match);
  const history = useHistory();
  const handleLogout = async () => {
    const result = await authApi.logout(dataUser._id);
    localStorage.removeItem("dataLogin");
    history.push("/login");
  };

  useEffect(() => {
    const dataLogin = JSON.parse(localStorage.getItem("dataLogin"));
    console.log("dataLogin====", dataLogin);
    setDataUser(dataLogin);
  }, []);

  return (
    <Header className="header" style={{ height: 72, background: "#2a8ecf" }}>
      <Row style={{ height: 72 }}>
        <Col span={16} style={{ height: "100%" }}>
          <div
            className="logo"
            style={{ display: "flex", alignItems: "center" }}
          >
            <Link to="/admin">
              <img className={styles.logo} src={driving_log} />
            </Link>

            <h3 className={styles.text_header}>Driving Test Booking</h3>
          </div>
        </Col>
        <Col span={8} style={{ height: "100%", textAlign: "right" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "end",
              alignItems: "center",
              height: 72,
            }}
          >
            <p style={{ color: "white", marginBottom: 0, fontWeight: "bold" }}>
              {dataUser ? `${dataUser.first_name} ${dataUser.last_name}` : ""}
            </p>
            <button className={styles.btn_login} onClick={handleLogout}>
              Logout
            </button>
          </div>
          {/* <button className={styles.btn_login} onClick={handleLogout}>
            Logout
          </button> */}
        </Col>
      </Row>
    </Header>
  );
}
