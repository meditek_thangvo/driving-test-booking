// @ts-nocheck
import React, { useState, useEffect, useRef } from "react";
import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Typography } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import bookingApi from "../../api/bookingApi";
import socketIOClient from "socket.io-client";

const { SubMenu } = Menu;
const { Sider } = Layout;
const { Text } = Typography;

const host = "http://localhost:8000";

const Wrapper = styled.div`
  .ant-layout-sider-trigger {
    background-color: #2a8ecf;
    height: 32px;
    display: flex;
    justify-content: center;
    align-items: center;
    bottom: 32px;
  }
  .ant-layout-sider {
    background-color: #eff2f5 !important;
  }
`;

function SiderBar() {
  const [collapsed, setCollapsed] = useState(false);
  const [isStart, setIsStart] = useState(false);
  const socketRef = useRef();
  const onCollapse = (collapsed) => {
    setCollapsed(collapsed);
  };

  useEffect(() => {
    socketRef.current = socketIOClient.connect(host);

    socketRef.current.on("LogEvent", (dataGot) => {
      console.log("dataGot================", dataGot);
    });
  }, []);

  const handleToggleButton = async () => {
    let result;
    if (isStart) {
      result = await bookingApi.stopPDA();
    } else {
      result = await bookingApi.startPDA();
    }

    setIsStart(!isStart);
  };

  return (
    <Wrapper>
      <Sider
        collapsible
        collapsed={collapsed}
        width={180}
        onCollapse={onCollapse}
      >
        <Menu
          mode="inline"
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          style={{ height: "100%", borderRight: 0 }}
        >
          <SubMenu
            key="sub_candidate"
            icon={<UserOutlined />}
            title="Candidates"
          >
            <Menu.Item key="1">
              <Link to="/admin/candidate">Candidate list</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/admin/candidate/create">Create candidate</Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu
            key="sub_booking"
            icon={<LaptopOutlined />}
            title="Booking time"
          >
            <Menu.Item key="3">
              <Link to="/admin/booking-times">Booking times</Link>
            </Menu.Item>
            <Menu.Item key="4" onClick={handleToggleButton}>
              {isStart === false ? (
                <Text
                  style={{
                    color: "green",
                    fontWeight: "bold",
                  }}
                >
                  Start PDA
                </Text>
              ) : (
                <Text
                  style={{
                    color: "red",
                    fontWeight: "bold",
                  }}
                >
                  Stop PDA
                </Text>
              )}
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
    </Wrapper>
  );
}

export default SiderBar;
