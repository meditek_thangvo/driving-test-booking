import * as React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import BookingTimeList from "../components/booking/BookingTimeList";

export default function BookingFeatures() {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={match.path} exact>
        <BookingTimeList />
      </Route>
    </Switch>
  );
}
