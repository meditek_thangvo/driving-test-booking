import * as React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import CandidateList from "../components/candidate/CandidateList";
import CreateUpdateCandidate from "../components/candidate/CreateUpdateCandidate";

export default function CandidateFeatures() {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={match.path} exact>
        <CandidateList />
      </Route>

      <Route path={`${match.path}/create`} exact>
        <CreateUpdateCandidate />
      </Route>

      <Route path={`${match.path}/:candidateId`} exact>
        <CreateUpdateCandidate />
      </Route>
    </Switch>
  );
}
