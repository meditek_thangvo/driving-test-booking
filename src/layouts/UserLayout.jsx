import { Layout } from "antd";
import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import HeaderBar from "../components/header/HeaderBar";
import SiderBar from "../components/siderbar/SiderBar";
import BookingFeatures from "../layouts/BookingFeatures";
import CandidateFeatures from "../layouts/CandidateFeatures";
import ContentPanel from "../components/content/ContentPanel";
//@ts-ignore
import styles from "./UserLayout.module.less";

const { Content, Header, Footer } = Layout;

function UserLayout() {
  const match = useRouteMatch();
  console.log("match chinh", match);

  return (
    <Layout>
      <HeaderBar />
      <Layout>
        <SiderBar />
        <Content className={styles.container}>
          <Switch>
            {/* candidate */}
            <Route path="/admin/candidate">
              <CandidateFeatures />
            </Route>
            {/* booking time */}
            <Route exact path="/admin/booking-times">
              <BookingFeatures />
            </Route>
            <Route path="/admin">
              <ContentPanel />
            </Route>
          </Switch>
        </Content>
      </Layout>
      <div
        style={{
          height: 32,
          backgroundColor: "#eff2f5",
          position: "fixed",
          bottom: 0,
          left: 0,
          width: "100%",
        }}
      ></div>
    </Layout>
  );
}

export default UserLayout;
