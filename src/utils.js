import { notification } from "antd";

export const notificationFunction = (_type, _message) => {
  if (_type === "success") {
    return notification.success({
      message: _message,
      placement: "topRight",
      duration: 5,
    });
  } else if (_type === "error") {
    return notification.error({
      message: _message,
      placement: "topRight",
      duration: 5,
    });
  }
};
